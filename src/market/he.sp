
RegConsoleCmd("he", BUY_HE)

public Action:BUY_HE(client, args)
{
	if(args > 0)
	{
		PrintToConsole(client, "[SM] Usage: he");
		return Plugin_Handled;
	}

	new String:Arg1[32],
		String:name[32];

	GetCmdArg(1, Arg1, sizeof(Arg1));
	GetClientName(client, name, sizeof(name));

	PrintToChatAll("%s: %s", name, Arg1);
}